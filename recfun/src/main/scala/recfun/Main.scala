package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }

  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int = {
      if (c == 0 || r == 0) 1
      else if (c == r) 1
      else pascal(c, r - 1) + pascal(c - 1, r - 1)
    }


  def balanceRec(chars: List[Char], res: Int): Boolean =
    if (chars.isEmpty || res < 0) res == 0
    else if (chars.head == '(') balanceRec(chars.tail, res + 1)
    else if (chars.head == ')') balanceRec(chars.tail, res - 1)
    else balanceRec(chars.tail, res)

  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean =
      if (chars.isEmpty) true
      else balanceRec(chars, 0)

  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int = {
      def countChangeRec(money: Int, coins: List[Int]): Int =
        if (coins.isEmpty || money < 0) 0
        else if (money == 0) 1
        else countChangeRec(money - coins.head, coins) + countChangeRec(money, coins.tail)

      if (money == 0) 0
      else countChangeRec(money, coins)
    }
  }
